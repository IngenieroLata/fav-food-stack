/* Constants */
export const LOAD = 'foodStack/places/LOAD'
export const UPDATE = 'foodStack/places/UPDATE'

/* Reducers */
export default function reducer(state = {}, action) {
  switch (action.type) {
    case LOAD:
      const places = action.payload

      const newPlaces = places.filter(({ id }) => !state[id]).reduce(
        (acc, place) => ({
          ...acc,
          [place.id]: {
            name: place.name,
            rating: place.rating || 0,
            types: place.types,
            geometry: place.geometry,
            lat: place.geometry.location.lat(),
            lng: place.geometry.location.lng(),
          },
        }),
        {}
      )

      return {
        ...state,
        ...newPlaces,
      }
    case UPDATE:
      const { id, newValues } = action.payload
      return {
        ...state,
        [id]: {
          ...state[id],
          ...newValues,
        },
      }
    default:
      return state
  }
}

/* Action Creatores */
export const loadPlaces = places => ({
  type: LOAD,
  payload: places,
})

export const updatePlace = (id, newValues) => ({
  type: UPDATE,
  payload: { id, newValues },
})
