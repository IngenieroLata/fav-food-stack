/* Constants */
export const ADD = 'foodStack/favorites/ADD'
export const REMOVE = 'foodStack/favorites/REMOVE'

/* Reducers */
export default function reducer(state = [], action) {
  switch (action.type) {
    case ADD:
      if (state.indexOf(action.payload) < 0) {
        return [...state, action.payload]
      }
      return state
    case REMOVE:
      return state.filter(id => id !== action.payload)
    default:
      return state
  }
}

/* Action Creatores */
export const addFavorite = id => ({
  type: ADD,
  payload: id,
})

export const removeFavorite = id => ({
  type: REMOVE,
  payload: id,
})
