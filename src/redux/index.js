import { combineReducers } from 'redux'

import places from './places'
import favorites from './favorites'
import markers from './markers'

export default combineReducers({
  places,
  favorites,
  markers,
})
