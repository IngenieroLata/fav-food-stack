/* Constants */
import { LOAD } from './places'

/* Reducers */
export default function reducer(state = [], action) {
  switch (action.type) {
    case LOAD:
      return action.payload.map(place => place.id)
    default:
      return state
  }
}
