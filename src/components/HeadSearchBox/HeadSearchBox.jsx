import React, { Component, Fragment } from 'react'
import { ReactComponent as Logo } from './logo.svg'
import { content, logo, searchbox } from './HeadSearchBox.module.css'

class HeadSearchBox extends Component {
  constructor(props) {
    super(props)

    this.state = {
      visible: false,
    }
  }
  onPlacesChanged = () => {
    if (this.props.onPlacesChanged) {
      this.props.onPlacesChanged(this.searchBox.getPlaces())
    }
  }

  componentDidMount() {
    const { map, googlemaps } = this.props
    map.controls[googlemaps.ControlPosition.TOP_LEFT].push(this.wrapper)
    googlemaps.event.addListenerOnce(map, 'tilesloaded', () => {
      setTimeout(() => {
        this.setState({ visible: true }, () => {
          this.searchBox = new googlemaps.places.SearchBox(this.textInput)
          this.searchBox.addListener('places_changed', this.onPlacesChanged)
        })
      }, 100)
    })
  }

  componentWillUnmount() {
    this.searchBox.removeListener('places_changed', this.onPlacesChanged)
  }

  render() {
    return (
      <div className={content} ref={element => (this.wrapper = element)}>
        {this.state.visible && (
          <Fragment>
            <div className={logo}>
              <Logo />
            </div>
            <div className={searchbox}>
              <input
                type="text"
                ref={element => (this.textInput = element)}
                placeholder="Restaurants near..."
              />
            </div>
          </Fragment>
        )}
      </div>
    )
  }
}

export default HeadSearchBox
