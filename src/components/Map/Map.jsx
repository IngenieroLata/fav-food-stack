import React, { PureComponent } from 'react'
import { connect } from 'react-redux'
import { createSelector } from 'reselect'
import GoogleMapReact from 'google-map-react'

import { loadPlaces, updatePlace } from '../../redux/places'
import { addFavorite, removeFavorite } from '../../redux/favorites'
import HeadSearchBox from '../HeadSearchBox'
import Marker from '../Marker'

class Map extends PureComponent {
  constructor(props) {
    super(props)

    this.state = {
      apiReady: false,
      map: null,
      googlemaps: null,
    }

    this.apiIsLoaded = this.apiIsLoaded.bind(this)
    this.onPlacesChanged = this.onPlacesChanged.bind(this)
    this.handleFavorite = this.handleFavorite.bind(this)
  }

  handleFavorite(id, { favorite, ...place }) {
    this.props.updatePlace(id, place)
    if (favorite) {
      this.props.addFavorite(id)
    } else {
      this.props.removeFavorite(id)
    }
  }

  apiIsLoaded({ map, maps }) {
    this.setState({
      apiReady: true,
      map,
      googlemaps: maps,
    })

    this.bindResizeListener(map, maps)
  }

  bindResizeListener(map, maps) {
    maps.event.addDomListenerOnce(map, 'idle', () => {
      maps.event.addDomListener(window, 'resize', () => {
        const bounds = this.getMapBounds(maps, this.props.places)
        map.fitBounds(bounds)
      })
    })
  }

  getMapBounds(googlemaps, places) {
    const bounds = new googlemaps.LatLngBounds()

    places.forEach(place => bounds.union(place.geometry.viewport))
    return bounds
  }

  onPlacesChanged(places) {
    if (places.length) {
      const { map, googlemaps } = this.state
      const bounds = this.getMapBounds(googlemaps, places)
      map.fitBounds(bounds)
      this.props.loadPlaces(places)
    }
  }

  render() {
    const { apiReady, googlemaps, map } = this.state
    return (
      <div className={this.props.className}>
        <GoogleMapReact
          options={{
            styles: [
              {
                featureType: 'poi.business',
                stylers: [{ visibility: 'off' }],
              },
            ],
          }}
          bootstrapURLKeys={{
            key: process.env.REACT_APP_GMAP,
            libraries: ['places'],
          }}
          defaultCenter={[52.5065, 13.1445]}
          defaultZoom={10}
          yesIWantToUseGoogleMapApiInternals
          onGoogleApiLoaded={this.apiIsLoaded}
        >
          {apiReady && (
            <HeadSearchBox
              map={map}
              googlemaps={googlemaps}
              onPlacesChanged={this.onPlacesChanged}
            />
          )}

          {this.props.places.map(place => (
            <Marker
              key={place.id}
              lat={place.lat}
              lng={place.lng}
              place={place}
              handleFavorite={this.handleFavorite}
            />
          ))}
        </GoogleMapReact>
      </div>
    )
  }
}

const placesSelector = createSelector(
  state => state.markers,
  state => state.favorites,
  state => state.places,
  (markers, favorites, places) =>
    markers.map(id => ({
      id,
      favorite: favorites.indexOf(id) > -1,
      ...places[id],
    }))
)

const mapStateToProps = state => ({ places: placesSelector(state) })

export default connect(
  mapStateToProps,
  { loadPlaces, updatePlace, addFavorite, removeFavorite }
)(Map)
