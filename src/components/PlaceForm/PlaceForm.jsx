import React, { PureComponent } from 'react'

import { controls, placeName, placeRating, errorMessages } from './PlaceForm.module.css'

const maxLen = 40
const decimalRegex = /^([0-4](\.[0-9]{1})?|5(\.0)?)$/g
const required = value => (!value ? 'You are missing a field :/' : null)
const length = value => (value.length > maxLen ? `No more than ${maxLen} chars, please :v` : null)
const decimalRange = value =>
  !decimalRegex.test(value.trim()) ? 'Your Rating is not valid, try 5.0 :)' : null

const validators = {
  customName: [required, length],
  rating: [decimalRange],
}

class PlaceForm extends PureComponent {
  constructor(props) {
    super(props)
    this.state = {
      customName: '',
      rating: '',
      errors: [],
    }

    this.handleSubmit = this.handleSubmit.bind(this)
    this.handleChange = this.handleChange.bind(this)
  }

  handleSubmit(evt) {
    evt.preventDefault()

    const errorList = Object.keys(validators).reduce((errors, name) => {
      const value = this.state[name]
      const res = validators[name].map(validator => validator(value)).filter(Boolean)
      return [...errors, ...res]
    }, [])

    this.setState({
      errors: errorList,
    })

    this.props.onSubmit({ valid: !errorList.length, errors: errorList, values: this.state })
  }

  handleChange(evt) {
    const { name, value } = evt.target
    this.setState({ [name]: value })
  }

  render() {
    const { errors } = this.state
    return (
      <form onSubmit={this.handleSubmit} noValidate autoComplete="off">
        <div className={controls}>
          <input
            type="text"
            name="customName"
            className={placeName}
            placeholder="Name"
            value={this.state.customName}
            onChange={this.handleChange}
          />
          <input
            type="text"
            name="rating"
            className={placeRating}
            placeholder="Rating"
            value={this.state.rating}
            onChange={this.handleChange}
          />
        </div>

        <ul className={errorMessages}>
          {errors.map(error => (
            <li key={error}>{error}</li>
          ))}
        </ul>

        <button type="submit" className="button">
          Favorite!
        </button>
      </form>
    )
  }
}

export default PlaceForm
