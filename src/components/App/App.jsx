import React from 'react'

import SidePane from '../SidePane'
import Map from '../Map'
import { content, wrapper, pane } from './App.module.css'

const App = () => (
  <div className={content}>
    <Map className={wrapper} />
    <SidePane className={pane} />
  </div>
)

export default App
