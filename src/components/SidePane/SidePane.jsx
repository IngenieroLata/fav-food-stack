import React, { PureComponent } from 'react'
import { connect } from 'react-redux'
import { createSelector } from 'reselect'

import { ReactComponent as Sort } from './sort.svg'
import {
  content,
  contentEmpty,
  head,
  sort,
  asc,
  desc,
  favoriteList,
  favorite,
} from './SidePane.module.css'

class SidePane extends PureComponent {
  constructor(props) {
    super(props)

    this.state = {
      sort: asc,
    }
    this.toggleSort = this.toggleSort.bind(this)
  }

  renderFavorites() {
    const { favorites } = this.props
    const { sort } = this.state
    const sorted = favorites.sort(function(a, b) {
      return sort === asc ? a.rating - b.rating : b.rating - a.rating
    })

    return sorted.map(place => (
      <li key={place.id} className={favorite}>
        <strong>{(+place.rating).toFixed(1)}</strong>
        <span>{place.customName || place.name}</span>
      </li>
    ))
  }

  toggleSort() {
    const { sort } = this.state
    const { favorites } = this.props

    if (favorites.length > 1) {
      this.setState({
        sort: sort === asc ? desc : asc,
      })
    }
  }

  render() {
    const { className, favorites } = this.props

    if (!favorites.length) {
      return (
        <div className={`${className} ${content} ${contentEmpty}`}>
          <h1>Not Favorites added yet</h1>
        </div>
      )
    }

    return (
      <div className={`${className} ${content}`}>
        <div className={head}>
          <h1>Favorites</h1>
          <div className={sort} onClick={this.toggleSort}>
            <Sort className={favorites.length > 1 ? this.state.sort : ''} />
          </div>
        </div>
        <ul className={favoriteList}>{this.renderFavorites()}</ul>
      </div>
    )
  }
}

const favoriteRestaurants = createSelector(
  state => state.favorites,
  state => state.places,
  (favorites, places) =>
    favorites.map(id => ({
      id,
      ...places[id],
    }))
)

const mapStateToProps = state => ({ favorites: favoriteRestaurants(state) })

export default connect(mapStateToProps)(SidePane)
