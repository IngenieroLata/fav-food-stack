import React, { PureComponent, Fragment } from 'react'

import PlaceForm from '../PlaceForm'
import { mark, head, close, content } from './Marker.module.css'
import { ReactComponent as GeoLocate } from './geolocate.svg'
import { ReactComponent as Close } from './close.svg'

class Marker extends PureComponent {
  constructor(props) {
    super(props)

    this.handleFavorite = this.handleFavorite.bind(this, props.place.id)
    this.handleSubmit = this.handleSubmit.bind(this)
    this.state = {
      active: false,
    }
  }

  handleFavorite(id, values = {}) {
    const { favorite } = this.props.place
    this.props.handleFavorite(id, { favorite: !favorite, ...values })
  }

  handleSubmit({ valid, values }) {
    if (valid) {
      this.handleFavorite(values)
    }
  }

  renderPlace(place) {
    const { types, favorite } = place
    if (favorite || types.indexOf('food') > -1 || types.indexOf('restaurant') > -1) {
      return (
        <Fragment>
          <h3 style={{ margin: '1.3em 0' }}>Rating: {(+place.rating).toFixed(1)}</h3>
          <button type="button" className="button" onClick={this.handleFavorite}>
            {favorite ? 'Un-Favorite' : 'Favorite!'}
          </button>
        </Fragment>
      )
    }

    return <PlaceForm onSubmit={this.handleSubmit} />
  }

  render() {
    const { place } = this.props
    return (
      <Fragment>
        <div className={mark} onClick={() => this.setState({ active: true })}>
          <GeoLocate />
        </div>
        {this.state.active && (
          <div className={content}>
            <div className={head}>
              <h2>{place.customName || place.name}</h2>
              <div className={close} onClick={() => this.setState({ active: false })}>
                <Close />
              </div>
            </div>
            {this.renderPlace(place)}
          </div>
        )}
      </Fragment>
    )
  }
}

export default Marker
