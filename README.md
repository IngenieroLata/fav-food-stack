# Fav Food Stack

This is the solution for the Test Case at [Delivery Hero](https://www.deliveryhero.com).

## Getting Started

No special CLI or any type of black magic is needed, so go ahead and clone this repository.

### Prerequisites

Although we are going to need `Node.js (v8.0+)`

### Quick Start

Install dependencies

```
$ yarn
```

And run as development

```
$ yarn start
```

Check out `http://localhost:3000`

### Build

Install dependencies

```
$ yarn
```

And build the project

```
$ yarn build
```

You can follow the instructions on the console and use [serve](https://github.com/zeit/serve)

```
serve -s build
```

## Built With

- [Create React App](https://github.com/facebook/create-react-app) - Handle the Module bundler configuration (webpack)
- [React](https://reactjs.org/) - UI Javascript library
- [Redux](https://redux.js.org/) - Predictable state container
- [Reselect](https://github.com/reduxjs/reselect) - Selector library for Redux
- [Google Map React](http://google-map-react.github.io/google-map-react/map/main/) - Google map library for react

Among others libraries, see the [package.json](package.json) file.

## Author

- **Diego Toro**

## License

This project is licensed under the MIT License - see the [LICENSE](LICENSE) file for details
